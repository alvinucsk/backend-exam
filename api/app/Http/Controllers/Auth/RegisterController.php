<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Models\User;
use App\Models\Users;

class RegisterController extends Controller {

    public function index() {
        $users = Users::paginate(20);
        return $users;
    }
    public function register(Request $request) {
        $validated = $this->validator($request->all());

        if($validated->fails()) {
            return response()->json([
                "message"   => "The given data was invalid.",
                "errors"      => $validated->messages()
            ], 422);
        }
        else {
            $user = $this->create($request->all());
            // Auth::login($user);
            $user->generateToken();
            return response()->json($user->toArray(), 201);
        }
    }
    protected function validator(array $data) {
        $validator = Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
        return $validator;
    }
    protected function create(array $data) {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
