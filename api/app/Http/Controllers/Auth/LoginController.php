<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class LoginController extends Controller {
    public function login(Request $request) {
        $validated = $this->validator($request->all());
        $response = NULL;
        // Check if form submitted is valid
        if($validated->fails()) {
            $response = response()->json([
                "message"   => "The given data was invalid.",
                "errors"    => $validated->messages()
            ], 422);
        }
        else {
            $response = $this->auth($request->email, $request->password);
        }
        return $response;
    }
    public function logout(Request $request) {
        $user = Auth::guard('api')->user();
        if ($user) {
            $user->api_token = null;
            $user->save();
        }
        return response()->json(['data' => 'User logged out.'], 200);
    }
    protected function validator(array $data) {
        $validator = Validator::make($data, [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ]);
        
        return $validator;
    }
    protected function auth($email, $pass) {
        if(Auth::attempt(["email" => $email, "password" => $pass])) {
            $user = Auth::user();
            if(!$user->api_token) {
                $user->generateToken();
            }
            return response()->json([
                'token' => $user->api_token,
                'expires_at' => $user->expiry,
                'token_type' => 'bearer'
            ], 200);
        }
        else {
            return response()->json([
                'message' => 'The given data was invalid.',
                'errors' => [ 'email' => ['These credentials do not match our records.'] ]
            ], 422);
        }
    }
}
