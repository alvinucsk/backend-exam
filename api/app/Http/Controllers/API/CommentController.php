<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Posts;
use App\Models\Comments;
use Illuminate\Support\Facades\Validator;
use Auth;

class CommentController extends Controller {
    public function index(String $slug) {
        try {
            $post = Posts::where('slug', $slug)->firstOrFail();
            $comments = Comments::where('commentable_id', $post->id)->paginate(20);
            return response()->json($comments, 200);
        } catch(\Throwable $e) {
            return response()->json(['data' => []], 200);
        }
    }
    public function store(Request $request, String $slug) {
        $validated = $this->validator($request->all());

        if($validated->fails()) {
            return response()->json([
                "message"   => "The given data was invalid.",
                "errors"      => $validated->messages()
            ], 422);
        }
        else {
            try {
                $post = Posts::where('slug', $slug)->firstOrFail();
                $user = Auth::user();
                $comment = Comments::create([
                    'body' => $request->body,
                    'creator_id' => $user->id,
                    'commentable_id' => $post->id,
                    'commentable_type' => get_class($post),
                    'parent_id' => null
                ]);

                return response()->json(['data' => $comment ], 201);
            } catch(\Throwable $e) {
                return response()->json(['message' => $e->getMessage()], 404);
            }
        }
    }
    public function update(Request $request, String $slug, Int $id) {
        $validated = $this->validator($request->all());
        if($validated->fails()) {
            return response()->json([
                "message"   => "The given data was invalid.",
                "errors"      => $validated->messages()
            ], 422);
        }
        else {
            try {
                $post = Posts::where('slug', $slug)->firstOrFail();
                $comments = Comments::where('id', $id)->firstOrFail();
                $comments->update($request->all());
                return response()->json(['data' => $comments], 200);
            } catch(\Throwable $e) {
                return response()->json(['message' => $e->getMessage()], 404);
            }
        }
    }
    public function destroy(Request $request, String $slug, Int $id) {
        // $post->delete();
        try {
            $post = Posts::where('slug', $slug)->firstOrFail();
            Comments::where('id', $id)->delete();
            return response()->json(['status' => 'record deleted successfully'], 200);
        } catch(\Throwable $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        }
    }
    protected function validator(array $data) {
        $validator = Validator::make($data, [
            'body' => 'required'
        ]);
        return $validator;
    }
}
