<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Posts;
use Illuminate\Support\Facades\Validator;
use Auth;

class PostController extends Controller {
    public function index() {
        $posts = Posts::paginate(20);
        return $posts;
    }
    public function store(Request $request) {
        $validated = $this->validator($request->all());
        if($validated->fails()) {
            return response()->json([
                "message"   => "The given data was invalid.",
                "errors"      => $validated->messages()
            ], 422);
        }
        else {
            $user = Auth::user();
            $post = Posts::create([
                'title' => $request->title,
                'content' => $request->content,
                'image' => $request->image,
                'slug' => Str::slug($request->title, '-'),
                'user_id' => $user->id
            ]);
            return response()->json(['data' => $post], 201);
        }
    }
    public function show(String $slug) {
        $post = Posts::where('slug', $slug)->firstOrFail();
        return response()->json(['data' => $post], 201);
    }
    public function update(Request $request, String $slug) {
        $post = Posts::where('slug', $slug)->firstOrFail();
        $post->update($request->all());
        return response()->json(['data' => $post], 200);
    }
    public function destroy(String $slug) {
        // $post->delete();
        Posts::where('slug', $slug)->delete();
        return response()->json(['status' => 'record deleted successfully'], 200);
    }
    protected function validator(array $data) {
        $validator = Validator::make($data, [
            'title' => 'required',
            'content' => 'required'
        ]);
        return $validator;
    }
}
