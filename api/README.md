# Backend API Exam #

## Initialization ##

** Run the migrate First before you can start the Web App **
- `php artisan migrate`


** Loading Data **
- Be sure to seed the UsersTableSeeder first to avoid errors on seeding the rest of the tables `php artisan db:seed --class=UsersTableSeeder`
- After seeding the `UsersTableSeeder` you can now run `php artisan db:seed`


** Running the app **
- you can now run the app using `php artisan serve`, be sure to have mysql service running the background
