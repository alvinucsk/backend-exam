<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate();
        $faker = \Faker\Factory::create();
        $password = Hash::make('alvinsAPIExam');

        User::create([
            'name'      => 'Administrator',
            'email'     => 'admin@alvin.com',
            'password'  => $password,
            'api_token' => Str::random(60),
            'expiry'    => Carbon::now()->addYears(3)
        ]);

        for ($i = 0; $i < 10; $i++) {
            User::create([
                'name'      => $faker->name,
                'email'     => $faker->email,
                'password'  => $password,
                'api_token' => Str::random(60),
                'expiry'    => Carbon::now()->addYears(3)
            ]);
        }
    }
}
