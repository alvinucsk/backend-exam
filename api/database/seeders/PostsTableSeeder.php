<?php

namespace Database\Seeders;

use App\Models\Posts;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Faker\Factory;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Posts::truncate();
        $faker = Factory::create();

        for ($i = 0; $i < 50; $i++) {
            $title = $faker->sentence;
            $image = $faker->image('public/storage/images/posts',640,480, null, false);
            Posts::create([
                'title' => $title,
                'content' => $faker->paragraph,
                'image' => URL::to('/').(Storage::url('images/posts/'.$image)),
                'slug' => Str::slug($title, '-'),
                'user_id' => User::all()->random()->id,
            ]);
        }
    }
}
